# TransLIBRAS

Projeto do Grupo Turing, que visa converter sinais da Língua Brasileira de Sinais em Português 


## Recursos utilizados

* Os datasets utilizados estão localizados no Drive: Grupo Turing 2018 > Projetos > Projetos em desenvolvimento > Libras > datasets


### Pré-requisitos

* Esse projeto utiliza tensorflow, keras, numpy, matplotlib e OpenCV, por exemplo. Recomenda-se a instalação pelo conda ou pip.


### Treinamento

Para realizar o treinamento da rede no Sprint 1, foi utilizado o arquivo ```CNN_libras_classifier_sprint1.ipynb``` localizado na pasta ```sprint1```.


## Real time testing

O arquivo de predição em tempo real está localizado em ```HandPoseEstimation > Prediction_hand_tracking.py```. As predições são visíveis tanto no terminal, quanto no na janela ```local_img```.

## Criar dataset

Para criar seu próprio dataset em real time, utilize o comando:
 ```python live_mask.py --name classe```
Onde "classe" deve ser substituído pela classe a ser criada.



## Autores

* **Arthur Bucker**
* **Guilherme Marques**
* **Lucas Sobrinho**
* **Matheus Guinezi**
* **Vinicius Nardelli**


## Reconhecimento

* [Convolutional Pose Machines](https://github.com/timctho/convolutional-pose-machines-tensorflow)