import numpy as np
import pandas as pd
import cv2
import os
from keras.utils import to_categorical

from keras.preprocessing.image import ImageDataGenerator
def load_dir(path):
    x = []
    y = []
    for f in os.listdir(path):
        if not f.endswith('.csv'):
            continue
        
        yval = ord(f[0].lower()) - ord('a')
        x.append(pd.read_csv(os.path.join(path, f), index_col=0))
        y.append([yval] * x[-1].shape[0])
    return np.concatenate(x).reshape(-1, 21, 2, 1), to_categorical(np.concatenate(y), num_classes=26)

class data_iterator:
    def __init__(self, X,Y,batch_size):
        self.i = 0
        if X.shape[0] == Y.shape[0]:
            raise "X and Y don't match"
        self.n = X.shape[0]
        batch_size
        print(self.n)
        self.data = data
    def __iter__(self):
        return self

    def next(self):
        next_i = max(self.i+self.batch_size, self.n)
        data = X[self.i:next_i],Y[self.i:next_i]
        if self.i < self.n:
            i = self.i
            self.i += self.batch_size
        return data

def ensemble_data_generator(img_dir,bg_img_dir,batch_size, input_size, agumentention_config=None, hnm=True, do_cropping=False):
    print("--------data_generator config----------")
    print("img_dir   :",img_dir)
    print("bg_img_dir:",bg_img_dir)
    print("batch_size:",batch_size)
    print("input_size:",input_size)
    print("----------------------------------------")

    data_X,data_Y = load_dir(img_dir)
    datagen = ImageDataGenerator(featurewise_center=True,
                                    featurewise_std_normalization=True,
                                    rotation_range=20,
                                    width_shift_range=0.2,
                                    height_shift_range=0.2,
                                    horizontal_flip=True)
    return data_iterator(data_X,data_Y,batch_size)

if __name__ == "__main__":
    from config import FLAGS
    g = ensemble_data_generator(FLAGS.train_img_dir,FLAGS.bg_img_dir, FLAGS.batch_size, FLAGS.input_size)
    print(g.next())