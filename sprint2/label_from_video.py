import cv2
from collections import deque
import os
import numpy as np
import pandas as pd


def save_label(label, path):
    df = pd.DataFrame(label, columns=['frame', 'letter'])
    df.to_csv(path)


video_dir = "./dataset"

video_index = 0
for f in os.listdir(video_dir):
    if not f[-3:] in ["mp4"]:
        continue
    video_path = video_dir + "/" + f
    cap = cv2.VideoCapture(video_path)
    # print(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
    label = []
    count = 0
    play = 0
    cv2.namedWindow("img")
    cv2.moveWindow("img", 20, 20)
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        k = cv2.waitKey(1)
        if k == 27:

            break
        if k in range(97, 123):  # ascii code for letters a to z
            # k -96 = labels from 1 to 26
            label.append([cap.get(1), k - 96])
            save_label(label, video_path[:-4] + ".csv")
            print(chr(k), "---", label)

        if k == ord(' '):
            if play == 0:
                play = 1
            else:
                play = 0
        if k == ord(','):
            cap.set(1, cap.get(1) - 1)
            play = 0
        if k == ord('.'):
            play = 0
            cap.set(1, cap.get(1) + 1)

        if play == -1:
            cap.set(1, cap.get(1) - 2)
        elif play == 0:
            cap.set(1, cap.get(1) - 1)

        # if record:
        #     cv2.circle(frame, (30, 30), 20, (0, 0, 200), -1)
        #     cv2.putText(frame, 'video_' + str(video_index), (100, 40),
        #                cv2.FONT_HERSHEY_SIMPLEX, 1, (20, 20, 20))
        #     count += 1
        cv2.imshow("img", frame)

    cap.release()
cv2.destroyAllWindows()
