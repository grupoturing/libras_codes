import cv2 as cv
from collections import deque
import os
import numpy as np
import pandas as pd


def save_label(label, path):
    df = pd.DataFrame(label)
    df.to_csv(path)


dataset_dir = "./dataset"

video_index = 0
for f in os.listdir(dataset_dir):
    if f[0] == 'v':
        video_index = max(video_index, int(f[-5]))


cap = cv.VideoCapture(0)
fourcc = cv.VideoWriter_fourcc(*'XVID')
out = cv.VideoWriter(dataset_dir + '/video_' + str(video_index) +
                     ".mp4", fourcc, 20.0, (640, 480))

label = np.array([])
record = False
count = 0

while cap.isOpened():
    ret, frame = cap.read()

    # detect_hand
    if record:
        out.write(frame)

    k = cv.waitKey(1)
    if k == 27:
        break
    if k in range(97, 123) and record:  # ascii code for letters a to z
        # k -96 = labels from 1 to 26
        label = np.append(label, [count, k - 96])
        save_label(label, dataset_dir + "/label" +
                   str(video_index) + ".csv")
        print(label)
    if k == ord(' '):
        record = not record
        print("record", record)
        if record:
            count = 0
            video_index += 1
            # creates a new file
            out = cv.VideoWriter(dataset_dir + '/video_' + str(video_index) +
                                 ".mp4", fourcc, 20.0, (640, 480))

    if record:
        cv.circle(frame, (30, 30), 20, (0, 0, 200), -1)
        cv.putText(frame, 'video_' + str(video_index), (100, 40),
                   cv.FONT_HERSHEY_SIMPLEX, 1, (20, 20, 20))
        count += 1
    cv.imshow("img", frame)


cap.release()
out.release()
cv.destroyAllWindows()
